package twocolorgraph;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Stack;

//Kalju Lee
//CS311
//Programming Assignment 2
//Two Colorable Graphs
//4/26/16
public class TwoColorGraph {

    final static int WHITE = 0;
    final static int GREEN = 1;
    final static int RED = -1;

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("you args me wrong");
            return;
        }

        Graph graph;

        try {
            // get the file to a scanner and get size
            Scanner scanner = new Scanner(new File(args[0]));
            graph = new Graph(scanner.nextInt());
            // get edges, add them going both ways
            while (scanner.hasNextInt()) {
                int to = scanner.nextInt() - 1;
                int from = scanner.nextInt() - 1;
                graph.addEdge(to, from);
                graph.addEdge(from, to);
            }
        } catch (Exception e) {
            System.err.println(e);
            System.err.println("getting graph go wrong");
            return;
        }
        // color that graph
        colorByDFS(graph);

        // output output
        try {
            PrintWriter writer = new PrintWriter(args[0] + "output", "UTF-8");
            String output = "";
            if (graph.twoColorable) {
                writer.println("Yes");
                for (int i = 0; i < graph.colors().length; i++)
                    writer.println((i + 1) + " " + (graph.colors()[i] == GREEN ? "Green" : "Red") );
            }
            else {
                writer.println("No");
                output = graph.badSubstructure();
                writer.println(output);
            }
            writer.close();
        } catch (Exception ex) {
            System.err.println("writing things go wrong");
        }
    }

    // colors the graph if colorable, else generates a 
    // string of a problematic cycle
    public static void colorByDFS(Graph graph) {
        for (int i = 0; i < graph.size(); i++) {
            if (graph.colors()[i] == WHITE) {
                graph.colors()[i] = GREEN;
                graph.depth()[i] = 0;
                Stack<Integer> stack = new Stack();
                stack.push(i);
                // while there are still nodes to explore, pops a node
                // and gets the adjacent nodes into the stack, DFS style
                while (!stack.isEmpty() && graph.twoColorable) {
                    int node = stack.pop();
                    // if the node has no edges, ignore it
                    if (graph.getEdgesFrom(node) == null) {
                        continue;
                    }
                    for (int otherNode : graph.getEdgesFrom(node)) {
                        // if the next node is newly discovered, update it's color,
                        // depth, predecessor, and put it in the stack
                        if (graph.colors()[otherNode] == WHITE) {
                            graph.colors()[otherNode] = graph.colors()[node] * -1;
                            graph.depth()[otherNode] = graph.depth()[node] + 1;
                            graph.predecessors()[otherNode] = node;
                            stack.push(otherNode);
                            // if the new node is the same color as the current one,
                            // then there is an odd cycle and the search is stopped.  
                            // twoColorable is set to false, and the bad substructure
                            // string is generated
                        } else if (graph.colors()[otherNode] == graph.colors()[node]) {
                            graph.twoColorable = false;
                            graph.findBadSubstructure(node, otherNode);
                            break;
                        }
                    }
                }
            }
        }
    }
}
