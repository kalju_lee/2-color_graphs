
package twocolorgraph;

import java.util.LinkedList;
import java.util.Stack;

//Kalju Lee
//CS311
//Programming Assignment 2
//Two Colorable Graphs
//4/26/16

public class Graph {

    private final int size;
    private final LinkedList<Integer>[] edges;
    private final int[] colors;
    private final int[] predecessors;
    private final int[] depth;
    boolean twoColorable;
    private String badSubstructure;

    // graph variables are initialized
    public Graph(int size) {
        this.size = size;
        edges = new LinkedList[size];
        colors = new int[size];
        predecessors = new int[size];
        twoColorable = true;
        depth = new int[size];
    }

    // if there are no edges present for the node,
    // a linked list is created.  
    // the edge is added to the list
    public void addEdge(int from, int to) {
        int index = from;
        if (edges[index] == null) {
            edges[index] = new LinkedList();
        }
        edges[index].add(to);
    }

    public LinkedList<Integer>[] getEdges() {
        return edges;
    }

    public LinkedList<Integer> getEdgesFrom(int node) {
        return edges[node];
    }

    public int[] colors() {
        return colors;
    }

    public int size() {
        return size;
    }

    public int[] predecessors() {
        return predecessors;
    }

    public String badSubstructure() {
        return badSubstructure;
    }

    public int[] depth() {
        return depth;
    }

    // takes two nodes, returns an odd cycle they are in
    public void findBadSubstructure(int node, int otherNode) {
        // create a stack for one node, a queue for the other
        // to make it easier to print them out at the end
        Stack<Integer> nodeList = new Stack();
        LinkedList<Integer> otherNodeList = new LinkedList();
        int currentNode = node;
        int currentOtherNode = otherNode;
        // if the nodes are not at the same depth, walks back up the 
        // cycle from one node till they are even, then continues to 
        // walk up from both nodes till a common parent is found
        while (currentNode != currentOtherNode) {
            switch (Integer.compare(depth[currentNode], depth[currentOtherNode])) {
                case 1: 
                    nodeList.push(currentNode);
                    currentNode = predecessors[currentNode];
                    break;
                case -1: 
                    otherNodeList.add(currentOtherNode);
                    currentOtherNode = predecessors[currentOtherNode];
                    break;
                case 0:
                    nodeList.push(currentNode);
                    otherNodeList.add(currentOtherNode);
                    currentNode = predecessors[currentNode];
                    currentOtherNode = predecessors[currentOtherNode];
                    break;
                default:
                    System.err.println("how did you get here?");
                    return;
            }
        }
        // builds an output string
        StringBuilder output = new StringBuilder();
        output.append(currentNode + 1);
        output.append(" ");
        while (!nodeList.isEmpty()) {
            output.append(nodeList.pop() + 1);
            output.append(" ");
        }
        while (!otherNodeList.isEmpty()) {
            output.append(otherNodeList.pop() + 1);
            if (!otherNodeList.isEmpty())
                output.append(" ");
        }
        badSubstructure = output.toString();
    }

}
